# Adding links to the LIMBO Deployment Infrastructure

Links that are generated should be based on datasets that are already released (i.e., they can be found in the metadata-catalog (<https://gitlab.com/limbo-project/metadata-catalog/tree/master/toLoad>). Released datasets are identified by their dct:identifier (i.e., <LIMBO_DATASET_ID> as source identifier und <LINKSET_TARGET_ID> as target identifier). The metadata catalog describes all released LIMBO datasets as well as a few well-known LOD datasets (e.g., DBpedia and Wikidata). In order to release a linkset do the following steps: 

1. Create a new folder in this repository named with the <LIMBO_DATASET_ID>. This represents the source dataset.
2. Copy the generated links into separate files, each named with the <LINKSET_TARGET_ID> into the <LIMBO_DATASET_ID> folder. They represent the target dataset(s).
3. Add the dataset-includes repository (<https://gitlab.com/limbo-project/dataset-includes> as a submodule to the <LIMBO_DATASET_ID> folder. 
4. Add a Makefile to the <LIMBO_DATASET_ID> folder containing the following path reference

~~~~
include dataset-includes/master/Makefile
~~~~

5. In order to start the release process for one linkset, create a new configuration file (config.sh) in the <LIMBO_DATASET_ID> folder with your parameters. For example:

~~~~
LIMBO_DATASET_ID=train_2-dataset
LINKSET_TARGET_ID=wikidata
~~~~

In your terminal, execute the following commands: 









